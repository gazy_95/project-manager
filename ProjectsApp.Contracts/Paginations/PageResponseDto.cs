﻿using ProjectsApp.Domain.Abstraction;
using System.Text.Json.Serialization;

namespace ProjectsApp.Contracts.Paginations;

/// <summary>
///     Page response model
/// </summary>
/// <typeparam name="T">Type of list elements</typeparam>
public class PageResponseDto<T>
{
    /// <summary>
    ///     Initializes a new instance of the <see cref="PageResponseDto{T}" /> class.
    /// </summary>
    public PageResponseDto(IPaginationRequest paginationRequest, int totalCount, List<T> result)
    {
        Pagination = new PaginationDto(totalCount, paginationRequest.PageNumber, paginationRequest.PageSize);
        List = result;
    }

    /// <summary>
    ///     Initializes a new instance of the <see cref="PageResponseDto{T}" /> class.
    /// </summary>
    /// <param name="pagination">Navigation information</param>
    /// <param name="list">List of elements</param>
    [JsonConstructor]
    public PageResponseDto(PaginationDto pagination, List<T> list)
    {
        Pagination = pagination;
        List = list;
    }

    /// <summary>
    ///     Navigation Information
    /// </summary>
    public PaginationDto Pagination { get; }

    /// <summary>
    ///     Array of received objects
    /// </summary>
    public List<T> List { get; }
}