﻿
namespace ProjectsApp.Contracts.Paginations;

/// <summary>
///     Navigation information
/// </summary>
public class PaginationDto
{
    /// <summary>
    ///     Initializes a new instance of the <see cref="PaginationDto" /> class.
    /// </summary>
    /// <param name="total">The total number of objects</param>
    /// <param name="pageNumber">Current page number</param>
    /// <param name="pageSize">The number of elements per page</param>
    public PaginationDto(int total, int pageNumber, int pageSize)
    {
        Total = total;
        PageNumber = pageNumber;
        PageSize = pageSize;
        PageCount = (total + pageSize - 1) / pageSize;
    }

    /// <summary>
    ///     The total number of objects
    /// </summary>
    public int Total { get; }

    /// <summary>
    ///     Number of pages
    /// </summary>
    public int PageCount { get; }

    /// <summary>
    ///     The number of elements per page
    /// </summary>
    public int PageSize { get; }

    /// <summary>
    ///     Current page number
    /// </summary>
    public int PageNumber { get; }
}
