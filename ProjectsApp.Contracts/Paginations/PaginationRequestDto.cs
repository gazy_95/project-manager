﻿using ProjectsApp.Domain.Abstraction;

namespace ProjectsApp.Contracts.Paginations;

/// <summary>
///     Navigation information
/// </summary>
public class PaginationRequestDto : IPaginationRequest
{
    /// <summary>
    ///     Navigation information
    /// </summary>
    public PaginationRequestDto()
    {
        PageSize = 10;
        PageNumber = 0;
    }

    /// <summary>
    ///     The number of elements per page
    /// </summary>
    public int PageSize { get; set; }

    /// <summary>
    ///     Current page number. 
    ///     The page number starts at 0.
    /// </summary>
    public int PageNumber { get; set; }
}
