﻿using ProjectsApp.Domain.Abstraction.ProjectParticipants;

namespace ProjectsApp.Contracts.ProjectParticipants;

/// <summary>
///   Project participants model dto
/// </summary>
public class ProjectParticipantsDto : IProjectParticipantsDetail
{ 
    /// <summary>
    ///   ProjectParticipants Id
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    ///   Project Id
    /// </summary>
    public int ProjectId { get; set; }

    /// <summary>
    ///   User Id
    /// </summary>
    public int UserId { get; set; }
}
