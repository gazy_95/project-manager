﻿
using ProjectsApp.Domain.Abstraction.Users;

namespace ProjectsApp.Contracts.Users;

/// <summary>
///   User detail model
/// </summary>
public class UserDetailDto : IUserShortInfo
{
    public UserDetailDto()
    {
        Name = string.Empty;
        Surname = string.Empty;
        Email = string.Empty;
    }

    /// <summary>
    ///  User Id
    /// </summary>
    public int Id { get; set; }

    /// <summary>
	///     Name
	/// </summary>
	public string Name { get; set; }

    /// <summary>
    ///     Surname
    /// </summary>
    public string Surname { get; set; }

    /// <summary>
    ///     Patronymic
    /// </summary>
    public string? Patronymic { get; set; }

    /// <summary>
    ///     Email
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    ///     Birth day
    /// </summary>
    public DateTime Birthday { get; set; }
}
