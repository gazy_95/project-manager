﻿using ProjectsApp.Domain.Abstraction.Users;

namespace ProjectsApp.Contracts.Users;

/// <summary>
///  User short info model
/// </summary>
public class UserShortInfoDto : IUserShortInfo
{
    public UserShortInfoDto()
    {
        Name =string.Empty;
        Surname = string.Empty;
        Email = string.Empty;
    }

    /// <summary>
	///     Name
	/// </summary>
	public string Name { get; set; }

    /// <summary>
    ///     Surname
    /// </summary>
    public string Surname { get; set; }

    /// <summary>
    ///     Patronymic
    /// </summary>
    public string? Patronymic { get; set; }

    /// <summary>
    ///     Email
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    ///     Birth day
    /// </summary>
    public DateTime Birthday { get; set; }
   
}
