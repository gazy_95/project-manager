﻿
using ProjectsApp.Domain.Abstraction.Projects;

namespace ProjectsApp.Contracts.Projects;

/// <summary>
///   Project detail info model dto
/// </summary>
public class ProjectDetailDto : IProjectDetail
{
	public ProjectDetailDto()
	{
		Name = string.Empty;
		СustomerCompanyName = string.Empty;
		ExecutorCompanyName = string.Empty;
	}

	/// <summary>
	///  Project Id
	/// </summary>
	public int Id { get; set; }

	/// <summary>
	/// Name
	/// </summary>
	public string Name { get; set; }

	/// <summary>
	///  Сustomer company name
	/// </summary>
	public string СustomerCompanyName { get; set; }

	/// <summary>
	///   Executor company name
	/// </summary>
	public string ExecutorCompanyName { get; set; }

	/// <summary>
	///   Priority
	/// </summary>
	public int Priority { get; set; }

	/// <summary>
	///   Start date
	/// </summary>
	public DateTime StartDate { get; set; }

	/// <summary>
	///   End date
	/// </summary>
	public DateTime EndDate { get; set; }

	/// <summary>
	///   Project manager Id
	/// </summary>
	public int ProjectManagerId { get; set; }
}
