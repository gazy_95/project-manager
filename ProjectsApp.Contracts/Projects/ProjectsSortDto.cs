﻿using ProjectsApp.Common.Enums;
using ProjectsApp.Domain.Abstraction.Projects;

namespace ProjectsApp.Contracts.Projects;

/// <summary>
///  Projects sort model dto
/// </summary>
public class ProjectsSortDto : IProjectsSort
{
	/// <summary>
	///     Is ascending sort type
	/// </summary>
	public bool Asc { get; set; }

	/// <summary>
	///  Sort by
	/// </summary>
	public ProjectSortingField SortBy { get; set; }
}
