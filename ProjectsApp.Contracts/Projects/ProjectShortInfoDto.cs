﻿using ProjectsApp.Domain.Abstraction.Projects;


namespace ProjectsApp.Contracts.Projects;

/// <summary>
///  Create project model dto
/// </summary>
public class ProjectShortInfoDto : IProjectShortInfo
{
	public ProjectShortInfoDto()
	{ 
		Name = string.Empty;
		СustomerCompanyName = string.Empty;
		ExecutorCompanyName = string.Empty;
	}

	/// <summary>
	/// Name
	/// </summary>
	public string Name { get; set; }

	/// <summary>
	///  Сustomer company name
	/// </summary>
	public string СustomerCompanyName { get; set; }

	/// <summary>
	///   Executor company name
	/// </summary>
	public string ExecutorCompanyName { get; set; }

	/// <summary>
	///   Priority
	/// </summary>
	public int Priority { get; set; }

	/// <summary>
	///   Start date
	/// </summary>
	public DateTime StartDate { get; set; }

	/// <summary>
	///   End date
	/// </summary>
	public DateTime EndDate { get; set; }

	/// <summary>
	///   Project manager Id
	/// </summary>
	public int ProjectManagerId { get; set; }
}
