﻿using ProjectsApp.Domain.Abstraction.Projects;

namespace ProjectsApp.Contracts.Projects;

/// <summary>
///  Projects filter model dto
/// </summary>
public class ProjectFilterDto : IProjectFilter
{
	/// <summary>
	///		Name
	/// </summary>
	public string? Name { get; set; }

	/// <summary>
	///     Priority
	/// </summary>
	public int? Priority { get; set; }

	/// <summary>
	///     Start date from
	/// </summary>
	public DateTime? StartDateFrom { get; set; }

	/// <summary>
	///     Start date to
	/// </summary>
	public DateTime? StartDateTo { get; set; }
}
