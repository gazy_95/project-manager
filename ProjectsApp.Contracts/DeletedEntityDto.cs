﻿

namespace ProjectsApp.Contracts;

/// <summary>
///     Deleted entity model
/// </summary>
/// <param name="Id">Deleted entity Id</param>
public record DeletedEntityDto(long Id);
