﻿using Microsoft.AspNetCore.Mvc;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.Paginations;
using ProjectsApp.Contracts.Projects;
using ProjectsApp.Service.Interfaces;
using System.Net;

namespace ProjectsApp.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class ProjectController : ControllerBase
{
    private readonly IProjectService _service;

    public ProjectController(IProjectService service)
    {
        _service = service;
    }

    /// <summary>
    ///     Get project by Id
    /// </summary>
    /// <param name="projectId">Project Id</param>
    [HttpGet("GetById/{projectId}")]
    [ProducesResponseType(typeof(ProjectDetailDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetByIdAsync([FromRoute] int projectId) =>
        Ok(await _service.GetProjectByIdAsync(projectId));

    /// <summary>
    ///     Create a new project
    /// </summary>
    /// <param name="model">Create project model</param>
    [HttpPost("Create")]
    [ProducesResponseType(typeof(ProjectDetailDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> CreateAsync([FromBody] ProjectShortInfoDto model) =>
        Ok(await _service.CreateProjectAsync(model));

    /// <summary>
    ///     Update project
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <param name="model">Update project model</param>
    [HttpPut("Update/{projectId}")]
    [ProducesResponseType(typeof(ProjectDetailDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> UpdateAsync([FromRoute] int projectId, [FromBody] ProjectShortInfoDto model) =>
        Ok(await _service.UpdateProjectAsync(projectId, model));

    /// <summary>
    ///     Delete project
    /// </summary>
    /// <param name="projectId">Project Id</param>
    [HttpDelete("Delete/{projectId}")]
    [ProducesResponseType(typeof(DeletedEntityDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> DeleteAsync([FromRoute] int projectId) =>
        Ok(await _service.DeleteProjectAsync(projectId));

    /// <summary>
    ///     Get projects by filter with sorting
    /// </summary>
    /// <param name="filter">Project filter model</param>
    /// <param name="sort">Projects sort model</param>
    /// <param name="pagination">Pagination model</param>
    [HttpGet("by-filter")]
    [ProducesResponseType(typeof(PageResponseDto<ProjectDetailDto>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetByFilterAsync([FromQuery] ProjectFilterDto filter, [FromQuery] ProjectsSortDto sort, [FromQuery] PaginationRequestDto pagination) =>
        Ok(await _service.GetProjectsAsync(filter, sort, pagination));
}
