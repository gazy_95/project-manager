﻿using Microsoft.AspNetCore.Mvc;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.ProjectParticipants;
using ProjectsApp.Service.Interfaces;
using System.Net;

namespace ProjectsApp.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class ProjectParticipantsController : ControllerBase
{
    private readonly IProjectParticipantsService _service;

    public ProjectParticipantsController(IProjectParticipantsService service)
    {
        _service = service;
    }

    /// <summary>
    ///     Get projectParticipants by Id
    /// </summary>
    /// <param name="projectParticipantsId">ProjectParticipants Id</param>
    [HttpGet("GetById/{projectParticipantsId}")]
    [ProducesResponseType(typeof(ProjectParticipantsDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetByIdAsync([FromRoute] int projectParticipantsId) =>
        Ok(await _service.GetProjectParticipantsByIdAsync(projectParticipantsId));

    /// <summary>
    ///   Create a new projectParticipants
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <param name="userId">User Id</param>
    [HttpPost("Create")]
    [ProducesResponseType(typeof(ProjectParticipantsDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> CreateAsync([FromRoute] int projectId, [FromRoute] int userId) =>
        Ok(await _service.CreateProjectParticipantsAsync(projectId, userId));

    /// <summary>
    ///     Delete projectParticipants
    /// </summary>
    /// <param name="projectParticipantsId">ProjectParticipants Id</param>
    [HttpDelete("Delete/{projectParticipantsId}")]
    [ProducesResponseType(typeof(DeletedEntityDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> DeleteAsync([FromRoute] int projectParticipantsId) =>
        Ok(await _service.DeleteProjectParticipantsAsync(projectParticipantsId));

    /// <summary>
    ///   Get all projectParticipants
    /// </summary>
    /// <returns>List projectParticipants</returns>
    [HttpGet("GetAllProjectParticipants")]
    [ProducesResponseType(typeof(List<ProjectParticipantsDto>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetAllProjectParticipantsAsync() =>
        Ok(await _service.GetAllProjectParticipantsAsync());

    /// <summary>
    ///   Get projectParticipants by user Id
    /// </summary>
    /// <param name="userId">User Id</param>
    /// <returns>List projectParticipants</returns>
    [HttpGet("GetProjectParticipantsByUserId")]
    [ProducesResponseType(typeof(List<ProjectParticipantsDto>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetProjectParticipantsByUserIdAsync([FromRoute] int userId) =>
        Ok(await _service.GetProjectParticipantsByUserIdAsync(userId));

    /// <summary>
    ///   Get projectParticipants by project Id
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <returns>List projectParticipants</returns>
    [HttpGet("GetProjectParticipantsByProjectId")]
    [ProducesResponseType(typeof(List<ProjectParticipantsDto>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetProjectParticipantsByProjectIdAsync([FromRoute] int projectId) =>
        Ok(await _service.GetProjectParticipantsByProjectIdAsync(projectId));
}
