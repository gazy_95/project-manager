﻿using Microsoft.AspNetCore.Mvc;
using ProjectsApp.Contracts;
using ProjectsApp.Service.Interfaces;
using System.Net;
using ProjectsApp.Contracts.Users;

namespace ProjectsApp.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly IUserService _service;

    public UserController(IUserService service)
    {
        _service = service;
    }

    [HttpGet("GetOK")]
    public IActionResult Get()
    {
        // Логика для обработки GET-запроса
        return Ok("Hello from C# API!");
    }



    /// <summary>
    ///     Get user by Id
    /// </summary>
    /// <param name="userId">User Id</param>
    [HttpGet("GetById/{userId}")]
    [ProducesResponseType(typeof(UserDetailDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetByIdAsync([FromRoute] int userId) =>
        Ok(await _service.GetUserByIdAsync(userId));

    /// <summary>
    ///     Add a new user
    /// </summary>
    /// <param name="model">Add user model</param>
    [HttpPost("Add")]
    [ProducesResponseType(typeof(UserDetailDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> AddAsync([FromBody] UserShortInfoDto model) =>
        Ok(await _service.AddUserAsync(model));

    /// <summary>
    ///     Update a user info
    /// </summary>
    /// <param name="userId">User Id</param>
    /// <param name="model">Update user model</param>
    [HttpPut("Update/{userId}")]
    [ProducesResponseType(typeof(UserDetailDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> UpdateAsync([FromRoute] int userId, [FromBody] UserShortInfoDto model) =>
        Ok(await _service.UpdateUserAsync(userId, model));

    /// <summary>
    ///     Delete a user
    /// </summary>
    /// <param name="userId">User Id</param>
    [HttpDelete("Delete/{userId}")]
    [ProducesResponseType(typeof(DeletedEntityDto), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> DeleteAsync([FromRoute] int userId) =>
        Ok(await _service.DeleteUserAsync(userId));

    /// <summary>
    ///   Get all users
    /// </summary>
    /// <returns>List users</returns>
    [HttpGet("GetAllUsers")]
    [ProducesResponseType(typeof(List<UserDetailDto>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetAllUsersAsync() =>
        Ok(await _service.GetAllUsersAsync());
}
