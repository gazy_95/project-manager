﻿using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Repository.Repository;
using ProjectsApp.Service.Interfaces;
using ProjectsApp.Service.Services;

namespace ProjectsApp.Applications;

/// <summary>
///   Initialize for repositories and services
/// </summary>
public static class Initialize
{
    /// <summary>
    ///  Initialize repositories
    /// </summary>
    public static void InitializeRepositories(this IServiceCollection services)
    {
        services.AddScoped<IProjectRepository, ProjectRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IProjectParticipantsRepository, ProjectParticipantsRepository>();
    }

    /// <summary>
    ///   Initialize services
    /// </summary>
    public static void InitializeServices(this IServiceCollection services)
    {
        services.AddScoped<IProjectService, ProjectService>();
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IProjectParticipantsService, ProjectParticipantsService>();
    }
}
