using Microsoft.EntityFrameworkCore;
using ProjectsApp.Applications;
using ProjectsApp.Repository;


namespace ProjectsApp.WebApi;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        var services = builder.Services;

        string connection = builder.Configuration.GetConnectionString("DefaultConnection");
        services.AddDbContext<ProjectsAppDbContext>(options => options.UseSqlServer(connection, b => b.MigrationsAssembly("ProjectsApp.Repository")));


        // Add services to the container.
        services.InitializeRepositories();
        services.InitializeServices();
        services.AddControllers();

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();

        services.AddCors(options =>
        {
            options.AddPolicy("VueCorsPolicy",
                builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Content-Disposition"));
        });

        var app = builder.Build();

        app.UseCors("VueCorsPolicy");

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();



        app.MapControllers();

        app.Run();
    }
}