﻿
using ProjectsApp.Contracts.Projects;
using ProjectsApp.Domain;

namespace ProjectsApp.Mappers.Projects;

/// <summary>
///   Project extension
/// </summary>
public static class ProjectExtension
{
	/// <summary>
	///     Map to project detail model
	/// </summary>
	/// <param name="project">Project</param>
	/// <returns>Project detail model</returns>
	public static ProjectDetailDto MapToProjectDetailDto(this Project project) =>
		new()
		{
			Id = project.Id,
			Name = project.Name,
			ExecutorCompanyName = project.ExecutorCompanyName,
			СustomerCompanyName = project.СustomerCompanyName,
			StartDate = project.StartDate,
			EndDate = project.EndDate,
			Priority = project.Priority,
			ProjectManagerId = project.ProjectManagerId
		};

}
