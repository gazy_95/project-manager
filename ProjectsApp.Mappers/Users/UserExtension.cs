﻿using ProjectsApp.Contracts.Users;
using ProjectsApp.Domain;

namespace ProjectsApp.Mappers.Users;

/// <summary>
///   User extension
/// </summary>
public static class UserExtension
{
    /// <summary>
	///     Map to user short info model
	/// </summary>
	/// <param name="user">User</param>
	/// <returns>User short info model</returns>
	public static UserDetailDto MapToUserDetailDto(this User user) =>
        new()
        {
            Id = user.Id,
            Name = user.Name,
            Surname = user.Surname,
            Email = user.Email,
            Birthday = user.Birthday,
            Patronymic = user.Patronymic
        };
}
