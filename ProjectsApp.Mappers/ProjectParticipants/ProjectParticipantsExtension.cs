﻿using ProjectsApp.Contracts.ProjectParticipants;


namespace ProjectsApp.Mappers.ProjectParticipants;

/// <summary>
///   ProjectParticipants extension
/// </summary>
public static class ProjectParticipantsExtension
{
    /// <summary>
	///     Map to project detail model
	/// </summary>
	/// <param name="project">Project</param>
	/// <returns>Project detail model</returns>
	public static ProjectParticipantsDto MapToProjectParticipantsDto(this ProjectsApp.Domain.ProjectParticipants projectParticipants) =>
        new()
        {
            Id = projectParticipants.Id,
            ProjectId = projectParticipants.ProjectId,
            UserId = projectParticipants.UserId
        };
}
