﻿

namespace ProjectsApp.Domain.Abstraction.Projects;

public interface IProjectDetail : IProjectShortInfo
{
	/// <summary>
	///  Project Id
	/// </summary>
	int Id { get; set; }
}
