﻿namespace ProjectsApp.Domain.Abstraction.Projects;

/// <summary>
///  Project short info model
/// </summary>
public interface IProjectShortInfo
{
	/// <summary>
	/// Name
	/// </summary>
	string Name { get; set; }

	/// <summary>
	///  Сustomer company name
	/// </summary>
	string СustomerCompanyName { get; set; }

	/// <summary>
	///   Executor company name
	/// </summary>
	string ExecutorCompanyName { get; set; }

	/// <summary>
	///   Priority
	/// </summary>
	int Priority { get; set; }

	/// <summary>
	///   Start date
	/// </summary>
	DateTime StartDate { get; set; }

	/// <summary>
	///   End date
	/// </summary>
	DateTime EndDate { get; set; }

	/// <summary>
	///   Project manager Id
	/// </summary>
	int ProjectManagerId { get; set; }
}
