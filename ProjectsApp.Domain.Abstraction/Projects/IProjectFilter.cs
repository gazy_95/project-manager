﻿
namespace ProjectsApp.Domain.Abstraction.Projects;

public interface IProjectFilter
{
	/// <summary>
	///		Name
	/// </summary>
	string? Name { get; set; }

	/// <summary>
	///     Priority
	/// </summary>
	int? Priority { get; set; }

	/// <summary>
	///     Start date from
	/// </summary>
	DateTime? StartDateFrom { get; set; }

	/// <summary>
	///     Start date to
	/// </summary>
	DateTime? StartDateTo { get; set; }
}

