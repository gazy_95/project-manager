﻿
using ProjectsApp.Common.Enums;

namespace ProjectsApp.Domain.Abstraction.Projects;

/// <summary>
///   Projects sort model
/// </summary>
public interface IProjectsSort
{
	/// <summary>
	///     Is ascending sort type
	/// </summary>
	bool Asc { get; set; }

	/// <summary>
	///  Sort by
	/// </summary>
	ProjectSortingField SortBy { get; set; }
}
