﻿namespace ProjectsApp.Domain.Abstraction.Users;

/// <summary>
///   User short info model
/// </summary>
public interface IUserShortInfo
{

	/// <summary>
	///     Name
	/// </summary>
	string Name { get; set; }

	/// <summary>
	///     Surname
	/// </summary>
	string Surname { get; set; }

	/// <summary>
	///     Patronymic
	/// </summary>
	string? Patronymic { get; set; }

    /// <summary>
    ///     Email
    /// </summary>
    string Email { get; set; }

    /// <summary>
    ///     Birth day
    /// </summary>
    DateTime Birthday { get; set; }
}
