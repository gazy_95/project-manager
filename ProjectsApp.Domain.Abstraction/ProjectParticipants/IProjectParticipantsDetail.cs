﻿namespace ProjectsApp.Domain.Abstraction.ProjectParticipants;

/// <summary>
///    ProjectParticipants model
/// </summary>
public interface IProjectParticipantsDetail
{
    /// <summary>
    ///  Id
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    ///  Project Id
    /// </summary>
    public int ProjectId { get; set; }

    /// <summary>
    ///   User Id
    /// </summary>
    public int UserId { get; set; }
}
