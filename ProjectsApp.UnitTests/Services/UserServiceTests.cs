﻿using Moq;
using ProjectsApp.Common.Enums;
using ProjectsApp.Common.Response;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.Users;
using ProjectsApp.Domain;
using ProjectsApp.Domain.Abstraction.Users;
using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Service.Services;

namespace ProjectsApp.UnitTests.Services;

/// <summary>
///  User service tests
/// </summary>
public class UserServiceTests
{
    [Fact]
    public async Task AddUserAsync_ValidUser_ReturnsUserDetailDto()
    {
        // Arrange
        var userRepository = new Mock<IUserRepository>();
        var userService = new UserService(userRepository.Object);
        var userShortInfo = new Mock<IUserShortInfo>();
      

        userRepository.Setup(db => db.AddUserAsync(userShortInfo.Object))
            .ReturnsAsync(new User());

        // Act
        var result = await userService.AddUserAsync(userShortInfo.Object);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.IsType<BaseResponse<UserDetailDto>>(result);
    }

    [Fact]
    public async Task UpdateUserAsync_ValidUser_ReturnsUpdatedUserDetailDto()
    {
        // Arrange
        var userRepository = new Mock<IUserRepository>();
        var userService = new UserService(userRepository.Object);
        var userShortInfo = new Mock<IUserShortInfo>();
        var userToUpdate = new User(); 
        var updatedUser = new User()
        {
            Id = userToUpdate.Id,
            Name = userToUpdate.Name,
            Surname = userToUpdate.Surname,
            Patronymic = userToUpdate.Patronymic,
            Email = userToUpdate.Email,
            Birthday = userToUpdate.Birthday
        };

        userRepository.Setup(db => db.GetRequiredUserByIdAsync(It.IsAny<int>())).ReturnsAsync(userToUpdate);
        userRepository.Setup(db => db.UpdateUserAsync(userToUpdate, userShortInfo.Object)).ReturnsAsync(updatedUser);

        // Act
        var result = await userService.UpdateUserAsync(1, userShortInfo.Object);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.IsType<BaseResponse<UserDetailDto>>(result);
        Assert.NotNull(result.Value);
    }

    [Fact]
    public async Task DeleteUserAsync_ValidUserId_ReturnsValidResponse()
    {
        // Arrange
        var repositoryMock = new Mock<IUserRepository>();
        var userService = new UserService(repositoryMock.Object);
        var userId = 1;
        var existingUser = new User();

        repositoryMock.Setup(db => db.GetRequiredUserByIdAsync(userId))
                   .ReturnsAsync(existingUser);

        repositoryMock.Setup(db => db.DeleteUserAsync(existingUser))
                   .Returns(Task.CompletedTask);

        // Act
        var result = await userService.DeleteUserAsync(userId);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<BaseResponse<DeletedEntityDto>>(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);

        repositoryMock.Verify(db => db.GetRequiredUserByIdAsync(userId), Times.Once);
        repositoryMock.Verify(db => db.DeleteUserAsync(existingUser), Times.Once);
    }

    [Fact]
    public async Task GetUserByIdAsync_ValidUserId_ReturnsValidResponse()
    {
        // Arrange
        var repositoryMock = new Mock<IUserRepository>();
        var userService = new UserService(repositoryMock.Object);
        var userId = 1;
        var existingUser = new User();

        repositoryMock.Setup(db => db.GetRequiredUserByIdAsync(userId))
                   .ReturnsAsync(existingUser);

        // Act
        var result = await userService.GetUserByIdAsync(userId);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<BaseResponse<UserDetailDto>>(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);

        repositoryMock.Verify(db => db.GetRequiredUserByIdAsync(userId), Times.Once);
    }

    [Fact]
    public async Task GetAllUsersAsync_ReturnsListOfUserDetailDto()
    {
        // Arrange
        var dbLayerMock = new Mock<IUserRepository>();
        var userService = new UserService(dbLayerMock.Object);

        var users = new List<User>
        {
            new User { Id = 1, Name = "User1" },
            new User { Id = 2, Name = "User2" }
        };
   
        dbLayerMock.Setup(db => db.GetAllUsersAsync()).ReturnsAsync(users);

        // Act
        var result = await userService.GetAllUsersAsync();

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);
        Assert.NotEmpty(result.Value);

        // Assuming MapToUserDetailDto is working correctly, check if the returned list has the same count as the input list
        Assert.Equal(users.Count, result.Value.Count);
    }

}
