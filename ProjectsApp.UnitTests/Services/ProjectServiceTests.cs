﻿using Moq;
using ProjectsApp.Common.Enums;
using ProjectsApp.Common.Response;
using ProjectsApp.Contracts.Projects;
using ProjectsApp.Domain.Abstraction.Projects;
using ProjectsApp.Domain;
using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Service.Services;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.Paginations;
using ProjectsApp.Domain.Abstraction;


namespace ProjectsApp.UnitTests.Services;

/// <summary>
///  Project service unit tests
/// </summary>
public class ProjectServiceTests
{

    [Fact]
    public async Task CreateProjectAsync_ValidProject_ReturnsValidResponse()
    {
        // Arrange
        var repositoryMock = new Mock<IProjectRepository>();
        var projectService = new ProjectService(repositoryMock.Object);
        var projectShortInfo = new Mock<IProjectShortInfo>();

        repositoryMock.Setup(db => db.CreateProjectAsync(projectShortInfo.Object))
                   .ReturnsAsync(new Project()); 
        // Act
        var result = await projectService.CreateProjectAsync(projectShortInfo.Object);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<BaseResponse<ProjectDetailDto>>(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);

        repositoryMock.Verify(db => db.CreateProjectAsync(projectShortInfo.Object), Times.Once);
    }
  
    [Fact]
    public async Task UpdateProjectAsync_ValidProject_ReturnsValidResponse()
    {
        // Arrange
        var repositoryMock = new Mock<IProjectRepository>();
        var projectService = new ProjectService(repositoryMock.Object);
        var projectId = 1; 
        var projectShortInfo = new Mock<IProjectShortInfo>();
        var existingProject = new Project();
        var updatedProject = new Project
        {
            Id = existingProject.Id,
            Name = existingProject.Name,
            ExecutorCompanyName = existingProject.ExecutorCompanyName,
            СustomerCompanyName = existingProject.СustomerCompanyName,
            StartDate = existingProject.StartDate,
            EndDate = existingProject.EndDate,
            ProjectManagerId = existingProject.ProjectManagerId,
            Priority = existingProject.Priority
        };

        repositoryMock.Setup(db => db.GetRequiredProjectByIdAsync(projectId))
                   .ReturnsAsync(existingProject);

        repositoryMock.Setup(db => db.UpdateProjectAsync(existingProject, projectShortInfo.Object))
                   .ReturnsAsync(updatedProject);

        // Act
        var result = await projectService.UpdateProjectAsync(projectId, projectShortInfo.Object);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<BaseResponse<ProjectDetailDto>>(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);

        repositoryMock.Verify(db => db.GetRequiredProjectByIdAsync(projectId), Times.Once);
        repositoryMock.Verify(db => db.UpdateProjectAsync(existingProject, projectShortInfo.Object), Times.Once);
    }

    [Fact]
    public async Task DeleteProjectAsync_ValidProjectId_ReturnsValidResponse()
    {
        // Arrange
        var repositoryMock = new Mock<IProjectRepository>();
        var projectService = new ProjectService(repositoryMock.Object);
        var projectId = 1; 
        var existingProject = new Project(); 

        repositoryMock.Setup(db => db.GetRequiredProjectByIdAsync(projectId))
                   .ReturnsAsync(existingProject);

        repositoryMock.Setup(db => db.DeleteProjectAsync(existingProject))
                   .Returns(Task.CompletedTask);

        // Act
        var result = await projectService.DeleteProjectAsync(projectId);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<BaseResponse<DeletedEntityDto>>(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);

        repositoryMock.Verify(db => db.GetRequiredProjectByIdAsync(projectId), Times.Once);
        repositoryMock.Verify(db => db.DeleteProjectAsync(existingProject), Times.Once);
    }

    [Fact]
    public async Task GetProjectByIdAsync_ValidProjectId_ReturnsValidResponse()
    {
        // Arrange
        var repositoryMock = new Mock<IProjectRepository>();
        var projectService = new ProjectService(repositoryMock.Object);
        var projectId = 1; 
        var existingProject = new Project(); 

        repositoryMock.Setup(db => db.GetRequiredProjectByIdAsync(projectId))
                   .ReturnsAsync(existingProject);

        // Act
        var result = await projectService.GetProjectByIdAsync(projectId);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<BaseResponse<ProjectDetailDto>>(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);

        repositoryMock.Verify(db => db.GetRequiredProjectByIdAsync(projectId), Times.Once);
    }

    [Fact]
    public async Task GetProjectsAsync_ValidProject_ReturnsValidResponse()
    {
        // Arrange
        var repositoryMock = new Mock<IProjectRepository>();
        var projectService = new ProjectService(repositoryMock.Object);
        var filter = new Mock<IProjectFilter>();
        var sort = new Mock<IProjectsSort>();
        var pagination = new Mock<IPaginationRequest>();
        var paginatedCollection = new List<ProjectDetailDto> 
        { 
            new ProjectDetailDto()
            {
                Id = 1,
                Name = "N1",
                ExecutorCompanyName = "C1",
                СustomerCompanyName = "C2",
                Priority = 1,
                ProjectManagerId = 2,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,  
            },
            new ProjectDetailDto()
            {
                Id = 2,
                Name = "N2",
                ExecutorCompanyName = "C2",
                СustomerCompanyName = "C2",
                Priority = 2,
                ProjectManagerId = 3,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
            }
        };
        var totalCount = 10;

        repositoryMock.Setup(db => db.GetProjectsByFilterAsync<ProjectDetailDto>(filter.Object, sort.Object, pagination.Object))
                   .ReturnsAsync((paginatedCollection, totalCount));

        // Act
        var result = await projectService.GetProjectsAsync(filter.Object, sort.Object, pagination.Object);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<BaseResponse<PageResponseDto<ProjectDetailDto>>>(result);

        repositoryMock.Verify(db => db.GetProjectsByFilterAsync<ProjectDetailDto>(filter.Object, sort.Object, pagination.Object), Times.Once);
    }
}
