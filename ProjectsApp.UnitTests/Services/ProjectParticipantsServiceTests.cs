﻿using Moq;
using ProjectsApp.Common.Enums;
using ProjectsApp.Domain;
using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Service.Services;

namespace ProjectsApp.UnitTests.Services;

public class ProjectParticipantsServiceTests
{
    [Fact]
    public async Task CreateProjectParticipantsAsync_UserAndProjectExist_ReturnsProjectParticipantsDto()
    {
        // Arrange
        var userRepositoryMock = new Mock<IUserRepository>();
        var projectRepositoryMock = new Mock<IProjectRepository>();
        var dbLayerMock = new Mock<IProjectParticipantsRepository>();
        var service = new ProjectParticipantsService(dbLayerMock.Object, projectRepositoryMock.Object, userRepositoryMock.Object);

        int projectId = 1;
        int userId = 1;
        var user = new User { Id = userId };
        var project = new Project { Id = projectId };
        var projectParticipants = new ProjectParticipants { ProjectId = projectId, UserId = userId };

        userRepositoryMock.Setup(repo => repo.GetRequiredUserByIdAsync(userId)).ReturnsAsync(user);
        projectRepositoryMock.Setup(repo => repo.GetRequiredProjectByIdAsync(projectId)).ReturnsAsync(project);
        dbLayerMock.Setup(db => db.CreateProjectParticipantsAsync(projectId, userId)).ReturnsAsync(projectParticipants);

        // Act
        var result = await service.CreateProjectParticipantsAsync(projectId, userId);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);
        Assert.Equal(projectId, result.Value.ProjectId);
        Assert.Equal(userId, result.Value.UserId);
    }

    [Fact]
    public async Task DeleteProjectParticipantsAsync_ExistingProjectParticipant_DeletesSuccessfully()
    {
        // Arrange
        var userRepositoryMock = new Mock<IUserRepository>();
        var projectRepositoryMock = new Mock<IProjectRepository>();
        var dbLayerMock = new Mock<IProjectParticipantsRepository>();
        var service = new ProjectParticipantsService(dbLayerMock.Object, projectRepositoryMock.Object, userRepositoryMock.Object);

        int projectParticipantsId = 1;
        var projectParticipants = new ProjectParticipants { Id = projectParticipantsId };

        dbLayerMock.Setup(db => db.GetRequiredProjectParticipantsByIdAsync(projectParticipantsId)).ReturnsAsync(projectParticipants);
        dbLayerMock.Setup(db => db.DeleteProjectParticipantsAsync(projectParticipants)).Returns(Task.CompletedTask);

        // Act
        var result = await service.DeleteProjectParticipantsAsync(projectParticipantsId);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);
        Assert.Equal(projectParticipantsId, result.Value.Id);

        dbLayerMock.Verify(db => db.GetRequiredProjectParticipantsByIdAsync(projectParticipantsId), Times.Once);
        dbLayerMock.Verify(db => db.DeleteProjectParticipantsAsync(projectParticipants), Times.Once);
    }

    [Fact]
    public async Task GetProjectParticipantsByIdAsync_ExistingProjectParticipant_ReturnsProjectParticipantsDto()
    {
        // Arrange
        var userRepositoryMock = new Mock<IUserRepository>();
        var projectRepositoryMock = new Mock<IProjectRepository>();
        var dbLayerMock = new Mock<IProjectParticipantsRepository>();
        var service = new ProjectParticipantsService(dbLayerMock.Object, projectRepositoryMock.Object, userRepositoryMock.Object);

        int projectParticipantsId = 1;
        var projectParticipants = new ProjectParticipants { Id = projectParticipantsId };

        dbLayerMock.Setup(db => db.GetRequiredProjectParticipantsByIdAsync(projectParticipantsId)).ReturnsAsync(projectParticipants);

        // Act
        var result = await service.GetProjectParticipantsByIdAsync(projectParticipantsId);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);
        Assert.Equal(projectParticipantsId, result.Value.Id);

        dbLayerMock.Verify(db => db.GetRequiredProjectParticipantsByIdAsync(projectParticipantsId), Times.Once);
    }

    [Fact]
    public async Task GetAllProjectParticipantsAsync_WhenCalled_ReturnsAllProjectParticipants()
    {
        // Arrange
        var userRepositoryMock = new Mock<IUserRepository>();
        var projectRepositoryMock = new Mock<IProjectRepository>();
        var dbLayerMock = new Mock<IProjectParticipantsRepository>();
        var service = new ProjectParticipantsService(dbLayerMock.Object, projectRepositoryMock.Object, userRepositoryMock.Object);

        var projectParticipantsList = new List<ProjectParticipants>
        {
            new ProjectParticipants { Id = 1, UserId = 2, ProjectId = 2 },
            new ProjectParticipants { Id = 2, UserId = 1, ProjectId = 1 }
        };

        dbLayerMock.Setup(db => db.GetAllProjectParticipantsAsync()).ReturnsAsync(projectParticipantsList);

        // Act
        var result = await service.GetAllProjectParticipantsAsync();

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.NotNull(result.Value);
        Assert.Equal(projectParticipantsList.Count, result.Value.Count);

        dbLayerMock.Verify(db => db.GetAllProjectParticipantsAsync(), Times.Once);
    }

    [Fact]
    public async Task GetProjectParticipantsByProjectIdAsync_WithValidProject_ReturnsParticipants()
    {
        // Arrange
        var userRepositoryMock = new Mock<IUserRepository>();
        var projectRepositoryMock = new Mock<IProjectRepository>();
        var dbLayerMock = new Mock<IProjectParticipantsRepository>();
        var service = new ProjectParticipantsService(dbLayerMock.Object, projectRepositoryMock.Object, userRepositoryMock.Object);
        int projectId = 1;
        var project = new Project(); 
        var participantsList = new List<ProjectParticipants> 
        {
            new ProjectParticipants { Id = 1, UserId = 2, ProjectId = 2 },
            new ProjectParticipants { Id = 2, UserId = 1, ProjectId = 1 }
        };

        projectRepositoryMock.Setup(repo => repo.GetRequiredProjectByIdAsync(projectId)).ReturnsAsync(project);
        dbLayerMock.Setup(db => db.GetProjectParticipantsByProjectIdAsync(projectId)).ReturnsAsync(participantsList);

        // Act
        var result = await service.GetProjectParticipantsByProjectIdAsync(projectId);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.Equal(participantsList.Count, result.Value.Count);
    }

    [Fact]
    public async Task GetProjectParticipantsByUserIdAsync_WithValidProject_ReturnsParticipants()
    {
        // Arrange
        var userRepositoryMock = new Mock<IUserRepository>();
        var projectRepositoryMock = new Mock<IProjectRepository>();
        var dbLayerMock = new Mock<IProjectParticipantsRepository>();
        var service = new ProjectParticipantsService(dbLayerMock.Object, projectRepositoryMock.Object, userRepositoryMock.Object);
        int userId = 1;
        var user = new User();
        var participantsList = new List<ProjectParticipants>
        {
            new ProjectParticipants { Id = 1, UserId = 2, ProjectId = 2 },
            new ProjectParticipants { Id = 2, UserId = 1, ProjectId = 1 }
        };

        userRepositoryMock.Setup(repo => repo.GetRequiredUserByIdAsync(userId)).ReturnsAsync(user);
        dbLayerMock.Setup(db => db.GetProjectParticipantsByUserIdAsync(userId)).ReturnsAsync(participantsList);

        // Act
        var result = await service.GetProjectParticipantsByUserIdAsync(userId);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(StatusCode.Ok, result.StatusCode);
        Assert.Equal(participantsList.Count, result.Value.Count);
    }
}
