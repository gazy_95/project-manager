﻿using ProjectsApp.Domain;
using ProjectsApp.Domain.Abstraction.Users;

namespace ProjectsApp.Repository.Abstraction.Interfaces;

/// <summary>
///  User repository
/// </summary>
public interface IUserRepository : IGenericRepository<User>
{
	/// <summary>
	///     Add a new user 
	/// </summary>
	/// <param name="newUSer">Model for new user</param>
	/// <returns>User</returns>
	Task<User> AddUserAsync(IUserShortInfo newUSer);

	/// <summary>
	///     Update an user 
	/// </summary>
	/// <param name="userToUpdate">User to update</param>
	/// <param name="model">Model for user</param>
	/// <returns>User</returns>
	Task<User> UpdateUserAsync(User userToUpdate, IUserShortInfo model);

	/// <summary>
	///     Delete an user
	/// </summary>
	/// <param name="userToDelete">User to delete</param>
	Task DeleteUserAsync(User userToDelete);

	/// <summary>
	///     Get an user by primary key
	/// </summary>
	/// <param name="id">Primary key</param>
	/// <returns>User</returns>
	Task<User> GetRequiredUserByIdAsync(int id);

	/// <summary>
	///     Get all users 
	/// </summary>
	/// <returns>Users</returns>
	Task<List<User>> GetAllUsersAsync();
}
