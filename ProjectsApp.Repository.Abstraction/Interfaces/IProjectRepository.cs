﻿using ProjectsApp.Domain;
using ProjectsApp.Domain.Abstraction;
using ProjectsApp.Domain.Abstraction.Projects;


namespace ProjectsApp.Repository.Abstraction.Interfaces;

/// <summary>
///  Project repository
/// </summary>
public interface IProjectRepository : IGenericRepository<Project>
{
	/// <summary>
	///     Create a new project 
	/// </summary>
	/// <param name="projectToCreate">Model for project</param>
	/// <returns>Project</returns>
	Task<Project> CreateProjectAsync(IProjectShortInfo projectToCreate);

	/// <summary>
	///     Update an project 
	/// </summary>
	/// <param name="projectToUpdate">Project to update</param>
	/// <param name="model">Model for project</param>
	/// <returns>Project</returns>
	Task<Project> UpdateProjectAsync(Project projectToUpdate, IProjectShortInfo model);

	/// <summary>
	///     Delete an project
	/// </summary>
	/// <param name="projectToDelete">Project to delete</param>
	Task DeleteProjectAsync(Project projectToDelete);

	/// <summary>
	///     Get an project by primary key
	/// </summary>
	/// <param name="id">Primary key</param>
	/// <returns>Project</returns>
	Task<Project> GetRequiredProjectByIdAsync(int id);

	/// <summary>
	///     Get  projects by filter(not deleted)
	/// </summary>
	/// <typeparam name="TDetailInfo">
	///     Type of appointment detail.
	///     Must implement the <see cref="IProjectDetail" /> interface.
	/// </typeparam>
	/// <param name="filter">Project filter model</param>
	/// <param name="sort">Project sort model</param>
	/// <param name="pagination">Pagination model</param>
	/// <returns>Pagination data(project info)</returns>
	Task<(List<TDetailInfo> PaginatedCollection, int TotalCount)> GetProjectsByFilterAsync<TDetailInfo>(IProjectFilter filter, IProjectsSort sort, IPaginationRequest pagination)
		where TDetailInfo : IProjectDetail, new();
}
