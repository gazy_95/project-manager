﻿using ProjectsApp.Domain;

namespace ProjectsApp.Repository.Abstraction.Interfaces;

/// <summary>
///   Project participants repository
/// </summary>
public interface IProjectParticipantsRepository : IGenericRepository<ProjectParticipants>
{
	/// <summary>
	///     Create a new project participants
	/// </summary>
	/// <param name="projectId">Project Id</param>
	/// <param name="userId">User Id</param>
	/// <returns>Project participants</returns>
	Task<ProjectParticipants> CreateProjectParticipantsAsync(int projectId, int userId);

	/// <summary>
	///     Delete an project participants
	/// </summary>
	/// <param name="projectParticipantsToDelete">Project participants to delete</param>
	Task DeleteProjectParticipantsAsync(ProjectParticipants projectParticipantsToDelete);

	/// <summary>
	///     Get an project participants by primary key
	/// </summary>
	/// <param name="projectParticipantsId">Primary key</param>
	/// <returns>Project participants</returns>
	Task<ProjectParticipants> GetRequiredProjectParticipantsByIdAsync(int projectParticipantsId);

	/// <summary>
	///     Get all project participants 
	/// </summary>
	/// <returns>ProjectParticipants</returns>
	Task<List<ProjectParticipants>> GetAllProjectParticipantsAsync();

	/// <summary>
	///     Get project participants by user Id
	/// </summary>
	/// <param name="userId">User Id</param>
	/// <returns>ProjectParticipants</returns>
	Task<List<ProjectParticipants>> GetProjectParticipantsByUserIdAsync(int userId);

	/// <summary>
	///     Get project participants by project Id
	/// </summary>
	/// <param name="projectId">Project Id</param>
	/// <returns>ProjectParticipants</returns>
	Task<List<ProjectParticipants>> GetProjectParticipantsByProjectIdAsync(int projectId);
}
