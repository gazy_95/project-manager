﻿using System.Linq.Expressions;

namespace ProjectsApp.Repository.Extensions;

/// <summary>
///     Extension for working with IQueryable
/// </summary>
internal static class QueryableExtension
{

	/// <summary>
	///     Sort rows
	/// </summary>
	/// <typeparam name="TKey">Sorting parameter key</typeparam>
	/// <typeparam name="TSource">Type of source</typeparam>
	/// <param name="rows">Rows to sorting</param>
	/// <param name="keySelector">Sorting parameter</param>
	/// <param name="isAscSorting">Sorting type</param>
	/// <returns>Sorted rows</returns>
	internal static IOrderedQueryable<TSource> SortRows<TSource, TKey>(this IQueryable<TSource> rows,
		Expression<Func<TSource, TKey>> keySelector, bool? isAscSorting)
		=> isAscSorting == true ? rows.OrderBy(keySelector) : rows.OrderByDescending(keySelector);
}
