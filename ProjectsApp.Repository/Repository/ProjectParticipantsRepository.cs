﻿using Microsoft.EntityFrameworkCore;
using ProjectsApp.Domain;
using ProjectsApp.Repository.Abstraction.Interfaces;

namespace ProjectsApp.Repository.Repository;

/// <summary>
///   Project participants repository
/// </summary>
public class ProjectParticipantsRepository : GenericRepository<ProjectParticipants>, IProjectParticipantsRepository
{
	public ProjectParticipantsRepository(ProjectsAppDbContext context) : base(context) { }

	/// <summary>
	///     Create a new project participants
	/// </summary>
	/// <param name="projectId">Project Id</param>
	/// <param name="userId">User Id</param>
	/// <returns>Project participants</returns>
	public async Task<ProjectParticipants> CreateProjectParticipantsAsync(int projectId, int userId)
	{
		var newProjectParticipants = new ProjectParticipants()
		{
			ProjectId = projectId,
			UserId = userId
		};

		await InsertAsync(newProjectParticipants);
		await Context.SaveChangesAsync();
		return newProjectParticipants;
	}

	/// <summary>
	///     Delete an project participants
	/// </summary>
	/// <param name="projectParticipantsToDelete">Project participants to delete</param>
	public async Task DeleteProjectParticipantsAsync(ProjectParticipants projectParticipantsToDelete)
	{
		Delete(projectParticipantsToDelete);
		await Context.SaveChangesAsync();
	}

	/// <summary>
	///     Get all project participants 
	/// </summary>
	/// <returns>ProjectParticipants</returns>
	public async Task<List<ProjectParticipants>> GetAllProjectParticipantsAsync() =>
		await AsQueryable().ToListAsync();

	/// <summary>
	///     Get project participants by project Id
	/// </summary>
	/// <param name="projectId">Project Id</param>
	/// <returns>ProjectParticipants</returns>
	public async Task<List<ProjectParticipants>> GetProjectParticipantsByProjectIdAsync(int projectId) =>
		await AsQueryable().Where(projectParticipants => projectParticipants.ProjectId == projectId)
		.Include(projectParticipants => projectParticipants.Project)
		.Include(projectParticipants => projectParticipants.Employee)
		.ToListAsync();


	/// <summary>
	///     Get project participants by user Id
	/// </summary>
	/// <param name="userId">User Id</param>
	/// <returns>ProjectParticipants</returns>
	public async Task<List<ProjectParticipants>> GetProjectParticipantsByUserIdAsync(int userId) =>
		await AsQueryable().Where(projectParticipants => projectParticipants.UserId == userId)
		.Include(projectParticipants => projectParticipants.Project)
		.Include(projectParticipants => projectParticipants.Employee)
		.ToListAsync();
	

	/// <summary>
	///     Get an project participants by primary key
	/// </summary>
	/// <param name="projectParticipantsId">Primary key</param>
	/// <returns>Project participants</returns>
	public async Task<ProjectParticipants> GetRequiredProjectParticipantsByIdAsync(int projectParticipantsId) =>
		await FirstOrDefaultAsync(projectParticipants => projectParticipants.Id == projectParticipantsId);
	
}
