﻿
using ProjectsApp.Domain;
using ProjectsApp.Domain.Abstraction.Projects;

namespace ProjectsApp.Repository.Repository.Utilities.Projects;

/// <summary>
///    Filter utility for projects
/// </summary>
internal static class ProjectsFilterUtility
{
	/// <summary>
	///     Apply filter
	/// </summary>
	/// <param name="queryable">Project queryable </param>
	/// <param name="filter">Filter model for getting projects</param>
	/// <returns>Filtered queryable collection</returns>
	internal static IQueryable<Project> ApplyFilter(this IQueryable<Project> queryable, IProjectFilter filter)
	{
		

		if (!string.IsNullOrEmpty(filter.Name))
			queryable = queryable.Where(project => project.Name.ToLower().Contains(filter.Name.ToLower()));

		if (filter.StartDateFrom.HasValue)
			queryable = queryable.Where(project => project.StartDate >= filter.StartDateFrom);

		if (filter.StartDateTo.HasValue)
			queryable = queryable.Where(project => project.StartDate <= filter.StartDateTo);

		if (filter.Priority.HasValue)
			queryable = queryable.Where(project => project.Priority == filter.Priority);

		return queryable;
	}
}
