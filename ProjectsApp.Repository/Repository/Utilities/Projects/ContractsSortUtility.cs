﻿using ProjectsApp.Common.Enums;
using ProjectsApp.Domain;
using ProjectsApp.Domain.Abstraction.Projects;
using ProjectsApp.Repository.Extensions;

namespace ProjectsApp.Repository.Repository.Utilities.Projects;

/// <summary>
///     Sort utility for projects
/// </summary>
internal static class ContractsSortUtility
{
	/// <summary>
	///     Apply sorting
	/// </summary>
	/// <param name="rows">Query model collection for short information of the contract</param>
	/// <param name="sort">Contracts sorting model</param>
	/// <returns>Sorted query model collection</returns>
	internal static IOrderedQueryable<Project> ApplySorting(this IQueryable<Project> rows, IProjectsSort sort)
		=> sort.SortBy switch
		{
			ProjectSortingField.Name => rows.SortRows(row => row.Name, sort.Asc),
			ProjectSortingField.СustomerCompanyName => rows.SortRows(row => row.СustomerCompanyName, sort.Asc),
			ProjectSortingField.ExecutorCompanyName => rows.SortRows(row => row.ExecutorCompanyName, sort.Asc),
			ProjectSortingField.Priority => rows.SortRows(row => row.Priority, sort.Asc),
			ProjectSortingField.StartDate => rows.SortRows(row => row.StartDate, sort.Asc),
			_ => rows.SortRows(row => row.Id, sort.Asc)
		};
}
