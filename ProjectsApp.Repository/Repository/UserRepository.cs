﻿using Microsoft.EntityFrameworkCore;
using ProjectsApp.Domain;
using ProjectsApp.Domain.Abstraction.Users;
using ProjectsApp.Repository.Abstraction.Interfaces;

namespace ProjectsApp.Repository.Repository;

/// <summary>
///  User repository
/// </summary>
public class UserRepository : GenericRepository<User>, IUserRepository
{
	public UserRepository(ProjectsAppDbContext context) : base(context) { }

	/// <summary>
	///   Add a new user
	/// </summary>
	/// <param name="newUSer">Model for user</param>
	/// <returns>User</returns>
	public async Task<User> AddUserAsync(IUserShortInfo newUser)
	{
		var addUser = new User()
		{
			Name = newUser.Name,
			Surname = newUser.Surname,
			Patronymic = newUser.Patronymic,
			Birthday = newUser.Birthday,
			CreatedDate = DateTime.Now,
			Email = newUser.Email
		};
		await InsertAsync(addUser);
		await Context.SaveChangesAsync();
		return addUser;
	}

	/// <summary>
	///   Delete an user
	/// </summary>
	/// <param name="userToDelete">User to delete</param>
	public async Task DeleteUserAsync(User userToDelete)
	{
		Delete(userToDelete);
		await Context.SaveChangesAsync();
	}

	/// <summary>
	///   Get all users
	/// </summary>
	/// <returns>Users</returns>
	public async Task<List<User>> GetAllUsersAsync() =>
		await AsQueryable().ToListAsync();

	/// <summary>
	///   Get required user by id
	/// </summary>
	/// <param name="id">User Id</param>
	/// <returns>User</returns>
	public async Task<User> GetRequiredUserByIdAsync(int id) =>
		await FirstOrDefaultAsync(type => type.Id == id);

	/// <summary>
	///    Update a user
	/// </summary>
	/// <param name="userToUpdate">User to update</param>
	/// <param name="model">Model for user</param>
	/// <returns>User</returns>
	public async Task<User> UpdateUserAsync(User userToUpdate, IUserShortInfo model)
	{
		userToUpdate.Name = model.Name;
		userToUpdate.Surname = model.Surname;
		userToUpdate.Patronymic = model.Patronymic;
		userToUpdate.Birthday = model.Birthday;
		userToUpdate.Email = model.Email;

		Update(userToUpdate);
		await Context.SaveChangesAsync();
		return userToUpdate;
	}
}
