﻿using ProjectsApp.Domain;
using ProjectsApp.Domain.Abstraction;
using ProjectsApp.Domain.Abstraction.Projects;
using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Repository.Repository.Utilities.Projects;

namespace ProjectsApp.Repository.Repository;

/// <summary>
///   Project repository
/// </summary>
public class ProjectRepository : GenericRepository<Project>, IProjectRepository
{
	public ProjectRepository(ProjectsAppDbContext context) : base(context) { }

	/// <summary>
	///		Create a new project
	/// </summary>
	/// <param name="projectToCreate">Model for create project</param>
	/// <returns>Project</returns>
	public async Task<Project> CreateProjectAsync(IProjectShortInfo projectToCreate)
	{
		var project = new Project()
		{
			Name = projectToCreate.Name,
			ExecutorCompanyName = projectToCreate.ExecutorCompanyName,
			СustomerCompanyName = projectToCreate.СustomerCompanyName,
			StartDate = projectToCreate.StartDate,
			EndDate = projectToCreate.EndDate,
			Priority = projectToCreate.Priority,
			ProjectManagerId = projectToCreate.ProjectManagerId

		};
		await InsertAsync(project);
		await Context.SaveChangesAsync();
		return project;
	}

	/// <summary>
	///   Delete an project
	/// </summary>
	/// <param name="projectToDelete">Project to delete</param>
	public async Task DeleteProjectAsync(Project projectToDelete)
	{
		Delete(projectToDelete);
		await Context.SaveChangesAsync();
	}

	/// <summary>
	///   Update an project
	/// </summary>
	/// <param name="projectToUpdate">Project to update</param>
	/// <param name="model">Model for project</param>
	/// <returns>Project</returns>
	public async Task<Project> UpdateProjectAsync(Project projectToUpdate, IProjectShortInfo model)
	{
		projectToUpdate.Name = model.Name;
		projectToUpdate.ExecutorCompanyName = model.ExecutorCompanyName;
		projectToUpdate.СustomerCompanyName = model.СustomerCompanyName;
		projectToUpdate.StartDate = model.StartDate;
		projectToUpdate.EndDate = model.EndDate;
		projectToUpdate.Priority = model.Priority;
		projectToUpdate.ProjectManagerId = model.ProjectManagerId;

		Update(projectToUpdate);
		await Context.SaveChangesAsync();
		return projectToUpdate;
	}

	/// <summary>
	///    Get required project by Id
	/// </summary>
	/// <param name="id">Project Id</param>
	/// <returns>Project</returns>
	public async Task<Project> GetRequiredProjectByIdAsync(int id) =>
		await FirstOrDefaultAsync(type => type.Id == id);


	/// <summary>
	///     Get projects by filter(not deleted)
	/// </summary>
	/// <typeparam name="TDetailInfo">
	///     Type of project detail.
	///     Must implement the <see cref="IProjectDetail" /> interface.
	/// </typeparam>
	/// <param name="filter">Project filter model</param>
	/// <param name="sort">Project sort model</param>
	/// <param name="pagination">Pagination model</param>
	/// <returns>Pagination data(project info)</returns>
	public async Task<(List<TDetailInfo> PaginatedCollection, int TotalCount)> GetProjectsByFilterAsync<TDetailInfo>(IProjectFilter filter, IProjectsSort sort, IPaginationRequest pagination) where TDetailInfo : IProjectDetail, new()
	{
		var projects = AsQueryable().ApplyFilter(filter).ApplySorting(sort);
		var (paginatedCollection, totalCount) = await ApplyPagination(projects, pagination.PageSize, pagination.PageNumber);

		return (paginatedCollection.Select(project => new TDetailInfo
		{
			Id = project.Id,
			Name = project.Name,
			ExecutorCompanyName = project.ExecutorCompanyName,
			СustomerCompanyName = project.СustomerCompanyName,
			Priority = project.Priority,
			StartDate = project.StartDate,
			EndDate = project.EndDate,
			ProjectManagerId = project.ProjectManagerId,
		}).ToList(), totalCount);
	}

}
