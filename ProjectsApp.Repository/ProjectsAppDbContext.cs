﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using ProjectsApp.Domain;
using Microsoft.EntityFrameworkCore;

namespace ProjectsApp.Repository;

/// <summary>
///     Projects app data base context
/// </summary>
public class ProjectsAppDbContext : IdentityDbContext<User, IdentityRole<int>, int>
{

	/// <summary>
	///     Initializes a new instance of the <see cref="ProjectsAppDbContext"/> class.
	/// </summary>
	/// <param name="options">Database context options</param>
	public ProjectsAppDbContext(DbContextOptions<ProjectsAppDbContext> options) : base(options) { }


	public DbSet<Project>? Projects { get; set; }

	public DbSet<User>? Users { get; set; }

	public DbSet<ProjectParticipants> ProjectParticipants { get; set; }

	/// <summary>
	///     Apply configurations 
	/// </summary>
	/// <param name="modelBuilder">Model builder</param>
	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);
		modelBuilder.Entity<ProjectParticipants>()
			.HasKey(ep => new { ep.UserId, ep.ProjectId });

		modelBuilder.Entity<ProjectParticipants>()
			.HasOne(ep => ep.Employee)
			.WithMany(e => e.AssignedProjects)
			.HasForeignKey(ep => ep.UserId)
			.OnDelete(DeleteBehavior.Restrict);

		modelBuilder.Entity<ProjectParticipants>()
			.HasOne(ep => ep.Project)
			.WithMany(p => p.AssignedEmployees)
			.HasForeignKey(project => project.ProjectId)
			.OnDelete(DeleteBehavior.Restrict);
	}
}
