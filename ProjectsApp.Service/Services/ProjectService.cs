﻿using ProjectsApp.Contracts.Projects;
using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Service.Interfaces;
using ProjectsApp.Mappers.Projects;
using ProjectsApp.Common.Response;
using ProjectsApp.Common.Response.Interfaces;
using ProjectsApp.Common.Enums;
using ProjectsApp.Contracts;
using ProjectsApp.Domain.Abstraction.Projects;
using ProjectsApp.Contracts.Paginations;
using ProjectsApp.Domain.Abstraction;


namespace ProjectsApp.Service.Services;

/// <summary>
///   Project service
/// </summary>
public class ProjectService : IProjectService
{
	private readonly IProjectRepository _dbLayer;

	public ProjectService(IProjectRepository dbLayer)
	{
		_dbLayer = dbLayer;
	}

	/// <summary>
	///   Create project 
	/// </summary>
	/// <param name="model">Model for create project</param>
	/// <returns>Project detail dto</returns>
	public async Task<IBaseResponse<ProjectDetailDto>> CreateProjectAsync(IProjectShortInfo model)
	{
		var baseResponse = new BaseResponse<ProjectDetailDto>();
		try
		{
			var project = await _dbLayer.CreateProjectAsync(model);
			baseResponse.Value = project.MapToProjectDetailDto();
			baseResponse.StatusCode = StatusCode.Ok;
			return baseResponse;
		}
		catch(Exception ex)
		{
			return new BaseResponse<ProjectDetailDto>()
			{
				Description = $"[CreateProjectAsync] : {ex.Message}",
				StatusCode = StatusCode.InternalServerError
			};
		}
	}

    /// <summary>
    ///   Delete an project
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <returns>Deleted entity dto</returns>
    public async Task<IBaseResponse<DeletedEntityDto>> DeleteProjectAsync(int projectId)
    {
		var baseResponse = new BaseResponse<DeletedEntityDto>();

        try
		{
            var existingProject = await _dbLayer.GetRequiredProjectByIdAsync(projectId);
            await _dbLayer.DeleteProjectAsync(existingProject);
            baseResponse.Value = new DeletedEntityDto(projectId);
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<DeletedEntityDto>()
            {
                Description = $"[DeleteProjectAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///   Get project by Id
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <returns>Project detail dto</returns>
    public async Task<IBaseResponse<ProjectDetailDto>> GetProjectByIdAsync(int projectId)
	{
		var baseResponse = new BaseResponse<ProjectDetailDto>();
		try
		{
			var project = await _dbLayer.GetRequiredProjectByIdAsync(projectId);
			baseResponse.Value = project.MapToProjectDetailDto();
			baseResponse.StatusCode = StatusCode.Ok;
			return baseResponse;
		}
		catch (Exception ex)
		{
			return new BaseResponse<ProjectDetailDto>()
			{
				Description = $"[GetProjectByIdAsync] : {ex.Message}",
				StatusCode = StatusCode.InternalServerError
			};
		}
	}

    /// <summary>
    ///   Update an project
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <param name="model">Model </param>
    /// <returns>Project detail dto</returns>
    public async Task<IBaseResponse<ProjectDetailDto>> UpdateProjectAsync(int projectId, IProjectShortInfo model)
    {
        var baseResponse = new BaseResponse<ProjectDetailDto>();
        try
        {
            var projectToUpdate = await _dbLayer.GetRequiredProjectByIdAsync(projectId);
            var project = await _dbLayer.UpdateProjectAsync(projectToUpdate, model);
            baseResponse.Value = project.MapToProjectDetailDto();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<ProjectDetailDto>()
            {
                Description = $"[UpdateProjectAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///     Get  projects by filter(not deleted)
    /// </summary>
    /// <param name="filter">Project filter model</param>
    /// <param name="sort">Project sort model</param>
    /// <param name="pagination">Pagination model</param>
    /// <returns>Pagination data(project info)</returns>
    public async Task<IBaseResponse<PageResponseDto<ProjectDetailDto>>> GetProjectsAsync(IProjectFilter filter, IProjectsSort sort, IPaginationRequest pagination)
    {
        var baseResponse = new BaseResponse<PageResponseDto<ProjectDetailDto>>();
        try
        {
            var (paginatedCollection, totalCount) = await _dbLayer.GetProjectsByFilterAsync<ProjectDetailDto>(filter, sort, pagination);
            baseResponse.Value =  new PageResponseDto<ProjectDetailDto>(pagination, totalCount, paginatedCollection);
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<PageResponseDto<ProjectDetailDto>>()
            {
                Description = $"[GetProjectsAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }
}
