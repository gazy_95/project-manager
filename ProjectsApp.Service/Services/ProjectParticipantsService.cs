﻿using ProjectsApp.Common.Response.Interfaces;
using ProjectsApp.Contracts.ProjectParticipants;
using ProjectsApp.Contracts;
using ProjectsApp.Service.Interfaces;
using ProjectsApp.Common.Enums;
using ProjectsApp.Common.Response;
using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Mappers.ProjectParticipants;



namespace ProjectsApp.Service.Services;

public class ProjectParticipantsService : IProjectParticipantsService
{
    private readonly IProjectParticipantsRepository _dbLayer;
    private readonly IProjectRepository _projectRepository;
    private readonly IUserRepository _userRepository;

    public ProjectParticipantsService(IProjectParticipantsRepository dbLayer, IProjectRepository projectRepository, IUserRepository userRepository)
    {
        _dbLayer = dbLayer;
        _projectRepository = projectRepository;
        _userRepository = userRepository;
    }

    /// <summary>
    ///   Create a project participants
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <param name="userId">User Id</param>
    /// <returns>ProjectParticipants model </returns>
    public async Task<IBaseResponse<ProjectParticipantsDto>> CreateProjectParticipantsAsync(int projectId, int userId)
    {
        var baseResponse = new BaseResponse<ProjectParticipantsDto>();
        try
        {
            var user = _userRepository.GetRequiredUserByIdAsync(userId);
            var project = _projectRepository.GetRequiredProjectByIdAsync(projectId);

            if(user != null && project != null) 
            {
                var projectParticipantsToCreate = await _dbLayer.CreateProjectParticipantsAsync(projectId, userId);
                baseResponse.Value = projectParticipantsToCreate.MapToProjectParticipantsDto();
                baseResponse.StatusCode = StatusCode.Ok;
                return baseResponse;
            }
            baseResponse.StatusCode = StatusCode.NotFound;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<ProjectParticipantsDto>()
            {
                Description = $"[CreateProjectParticipantsAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///  Delete a Project Participants
    /// </summary>
    /// <param name="projectParticipantsId">Project participants Id</param>
    public async Task<IBaseResponse<DeletedEntityDto>> DeleteProjectParticipantsAsync(int projectParticipantsId)
    {
        var baseResponse = new BaseResponse<DeletedEntityDto>();
        try
        {
            var projectParticipantsToDelete = await _dbLayer.GetRequiredProjectParticipantsByIdAsync(projectParticipantsId);
            await _dbLayer.DeleteProjectParticipantsAsync(projectParticipantsToDelete);
            baseResponse.Value = new DeletedEntityDto(projectParticipantsId);
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<DeletedEntityDto>()
            {
                Description = $"[DeleteProjectParticipantsAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///   Get ProjectParticipants by Id
    /// </summary>
    /// <param name="projectParticipantsId">ProjectParticipants Id</param>
    /// <returns>ProjectParticipants</returns>
    public async Task<IBaseResponse<ProjectParticipantsDto>> GetProjectParticipantsByIdAsync(int projectParticipantsId)
    {
        var baseResponse = new BaseResponse<ProjectParticipantsDto>();
        try
        {
            var projectParticipants = await _dbLayer.GetRequiredProjectParticipantsByIdAsync(projectParticipantsId);
            baseResponse.Value = projectParticipants.MapToProjectParticipantsDto();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<ProjectParticipantsDto>()
            {
                Description = $"[GetProjectParticipantsByIdAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///   Get all ProjectParticipants
    /// </summary>
    /// <returns>List ProjectParticipants</returns>
    public async Task<IBaseResponse<List<ProjectParticipantsDto>>> GetAllProjectParticipantsAsync()
    {
        var baseResponse = new BaseResponse<List<ProjectParticipantsDto>>();
        try
        {
            var projectParticipants = await _dbLayer.GetAllProjectParticipantsAsync();
            baseResponse.Value = projectParticipants.Select(projectParticipants => projectParticipants.MapToProjectParticipantsDto()).ToList();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<List<ProjectParticipantsDto>>()
            {
                Description = $"[GetAllProjectParticipantsAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///   Get list ProjectParticipants by user Id
    /// </summary>
    /// <returns>List ProjectParticipants</returns>
    public async Task<IBaseResponse<List<ProjectParticipantsDto>>> GetProjectParticipantsByUserIdAsync(int userId)
    {
        var baseResponse = new BaseResponse<List<ProjectParticipantsDto>>();
        try
        {
            var user = _userRepository.GetRequiredUserByIdAsync(userId);

            if(user == null)
            {
                baseResponse.StatusCode = StatusCode.NotFound;
                return baseResponse;
            }
            var projectParticipants = await _dbLayer.GetProjectParticipantsByUserIdAsync(userId);
            baseResponse.Value = projectParticipants.Select(projectParticipants => projectParticipants.MapToProjectParticipantsDto()).ToList();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<List<ProjectParticipantsDto>>()
            {
                Description = $"[GetProjectParticipantsByUserIdAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///   Get list ProjectParticipants by project Id
    /// </summary>
    /// <returns>List ProjectParticipants</returns>
    public async Task<IBaseResponse<List<ProjectParticipantsDto>>> GetProjectParticipantsByProjectIdAsync(int projectId)
    {
        var baseResponse = new BaseResponse<List<ProjectParticipantsDto>>();
        try
        {
            var project = _projectRepository.GetRequiredProjectByIdAsync(projectId);

            if (project == null)
            {
                baseResponse.StatusCode = StatusCode.NotFound;
                return baseResponse;
            }
            var projectParticipants = await _dbLayer.GetProjectParticipantsByProjectIdAsync(projectId);
            baseResponse.Value = projectParticipants.Select(projectParticipants => projectParticipants.MapToProjectParticipantsDto()).ToList();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<List<ProjectParticipantsDto>>()
            {
                Description = $"[GetProjectParticipantsByProjectIdAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }
}
