﻿using ProjectsApp.Common.Enums;
using ProjectsApp.Common.Response;
using ProjectsApp.Common.Response.Interfaces;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.Users;
using ProjectsApp.Domain.Abstraction.Users;
using ProjectsApp.Mappers.Users;
using ProjectsApp.Repository.Abstraction.Interfaces;
using ProjectsApp.Service.Interfaces;

namespace ProjectsApp.Service.Services;

/// <summary>
///  User service
/// </summary>
public class UserService : IUserService
{
    private readonly IUserRepository _dbLayer;

    public UserService(IUserRepository dbLayer)
    {
        _dbLayer = dbLayer;
    }

    /// <summary>
    ///  Add a new user
    /// </summary>
    /// <param name="model">Model for user</param>
    /// <returns>User detail info model</returns>
    public async Task<IBaseResponse<UserDetailDto>> AddUserAsync(IUserShortInfo model)
    {
        var baseResponse = new BaseResponse<UserDetailDto>();
        try
        {
            var user = await _dbLayer.AddUserAsync(model);
            baseResponse.Value = user.MapToUserDetailDto();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<UserDetailDto>()
            {
                Description = $"[AddUserAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///   Delete an user
    /// </summary>
    /// <param name="userId">User Id</param>
    /// <returns>Deleted entity dto</returns>
    public async Task<IBaseResponse<DeletedEntityDto>> DeleteUserAsync(int userId)
    {
        var baseResponse = new BaseResponse<DeletedEntityDto>();

        try
        {
            var userToDelete = await _dbLayer.GetRequiredUserByIdAsync(userId);
            await _dbLayer.DeleteUserAsync(userToDelete);
            baseResponse.Value = new DeletedEntityDto(userId);
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<DeletedEntityDto>()
            {
                Description = $"[DeleteUserAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///  Get all users
    /// </summary>
    /// <returns>List users</returns>
    public async Task<IBaseResponse<List<UserDetailDto>>> GetAllUsersAsync()
    {
        var baseResponse = new BaseResponse<List<UserDetailDto>>();
        try
        {
            var users = await _dbLayer.GetAllUsersAsync();

            baseResponse.Value = users.Select(user => user.MapToUserDetailDto()).ToList();
            baseResponse.StatusCode = StatusCode.Ok;

            return baseResponse;
        }
        catch (Exception e)
        {
            return new BaseResponse<List<UserDetailDto>>()
            {
                Description = $"[GetAllUsersAsync] : {e.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///  Get user by Id
    /// </summary>
    /// <param name="userId">User Id</param>
    /// <returns>User detail info</returns>
    public async Task<IBaseResponse<UserDetailDto>> GetUserByIdAsync(int userId)
    {
        var baseResponse = new BaseResponse<UserDetailDto>();
        try
        {
            var project = await _dbLayer.GetRequiredUserByIdAsync(userId);
            baseResponse.Value = project.MapToUserDetailDto();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<UserDetailDto>()
            {
                Description = $"[GetUserByIdAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }

    /// <summary>
    ///   Update an user
    /// </summary>
    /// <param name="userId">UserId</param>
    /// <param name="model">Model for user update</param>
    /// <returns>User detail info model</returns>
    public async Task<IBaseResponse<UserDetailDto>> UpdateUserAsync(int userId, IUserShortInfo model)
    {
        var baseResponse = new BaseResponse<UserDetailDto>();
        try
        {
            var userToUpdate = await _dbLayer.GetRequiredUserByIdAsync(userId);
            var user = await _dbLayer.UpdateUserAsync(userToUpdate, model);
            baseResponse.Value = user.MapToUserDetailDto();
            baseResponse.StatusCode = StatusCode.Ok;
            return baseResponse;
        }
        catch (Exception ex)
        {
            return new BaseResponse<UserDetailDto>()
            {
                Description = $"[UpdateUserAsync] : {ex.Message}",
                StatusCode = StatusCode.InternalServerError
            };
        }
    }
}
