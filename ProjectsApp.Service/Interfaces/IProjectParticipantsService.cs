﻿using ProjectsApp.Common.Response.Interfaces;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.ProjectParticipants;


namespace ProjectsApp.Service.Interfaces;

/// <summary>
///   Project participants service
/// </summary>
public interface IProjectParticipantsService
{
    /// <summary>
    ///   Create a project participants
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <param name="userId">User Id</param>
    /// <returns>ProjectParticipants model </returns>
    Task<IBaseResponse<ProjectParticipantsDto>> CreateProjectParticipantsAsync(int projectId, int userId);

    /// <summary>
    ///  Delete a Project Participants
    /// </summary>
    /// <param name="projectParticipantsId">Project participants Id</param>
    Task<IBaseResponse<DeletedEntityDto>> DeleteProjectParticipantsAsync(int projectParticipantsId);

    /// <summary>
    ///   Get ProjectParticipants by Id
    /// </summary>
    /// <param name="projectParticipantsId">ProjectParticipants Id</param>
    /// <returns>ProjectParticipants</returns>
    Task<IBaseResponse<ProjectParticipantsDto>> GetProjectParticipantsByIdAsync(int projectParticipantsId);

    /// <summary>
    ///   Get all ProjectParticipants
    /// </summary>
    /// <returns>List ProjectParticipants</returns>
    Task<IBaseResponse<List<ProjectParticipantsDto>>> GetAllProjectParticipantsAsync();

    /// <summary>
    ///   Get list ProjectParticipants by user Id
    /// </summary>
    /// <returns>List ProjectParticipants</returns>
    Task<IBaseResponse<List<ProjectParticipantsDto>>> GetProjectParticipantsByUserIdAsync(int userId);

    /// <summary>
    ///   Get list ProjectParticipants by project Id
    /// </summary>
    /// <returns>List ProjectParticipants</returns>
    Task<IBaseResponse<List<ProjectParticipantsDto>>> GetProjectParticipantsByProjectIdAsync(int projectId);
}
