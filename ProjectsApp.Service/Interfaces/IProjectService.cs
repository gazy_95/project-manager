﻿using ProjectsApp.Common.Response.Interfaces;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.Paginations;
using ProjectsApp.Contracts.Projects;
using ProjectsApp.Domain.Abstraction;
using ProjectsApp.Domain.Abstraction.Projects;

namespace ProjectsApp.Service.Interfaces;

/// <summary>
///   Project service 
/// </summary>
public interface IProjectService 
{
	/// <summary>
	///   Create an project service
	/// </summary>
	/// <param name="model">Model for create project</param>
	/// <returns>Project detail dto</returns>
	Task<IBaseResponse<ProjectDetailDto>> CreateProjectAsync(IProjectShortInfo model);

	/// <summary>
	///   Get project by id service
	/// </summary>
	/// <param name="projectId">Project Id</param>
	/// <returns>Project detail dto</returns>
	Task<IBaseResponse<ProjectDetailDto>> GetProjectByIdAsync(int projectId);

    /// <summary>
    ///   Delete an project
    /// </summary>
    /// <param name="projectId">Project Id</param>
    /// <returns>Deleted entity dto</returns>
    Task<IBaseResponse<DeletedEntityDto>> DeleteProjectAsync(int projectId);

	/// <summary>
	///   Update an project
	/// </summary>
	/// <param name="projectId">Project Id</param>
	/// <param name="model">Model for project</param>
	/// <returns></returns>
    Task<IBaseResponse<ProjectDetailDto>> UpdateProjectAsync(int projectId, IProjectShortInfo model);

    /// <summary>
    ///     Get  projects by filter(not deleted)
    /// </summary>
    /// <param name="filter">Project filter model</param>
    /// <param name="sort">Project sort model</param>
    /// <param name="pagination">Pagination model</param>
    /// <returns>Pagination data(project info)</returns>
    Task<IBaseResponse<PageResponseDto<ProjectDetailDto>>> GetProjectsAsync(IProjectFilter filter, IProjectsSort sort, IPaginationRequest pagination);
}
