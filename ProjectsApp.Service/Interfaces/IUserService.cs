﻿using ProjectsApp.Common.Response.Interfaces;
using ProjectsApp.Contracts;
using ProjectsApp.Contracts.Users;
using ProjectsApp.Domain.Abstraction.Users;

namespace ProjectsApp.Service.Interfaces;

/// <summary>
///  User service
/// </summary>
public interface IUserService
{
    /// <summary>
    ///  Add a new user
    /// </summary>
    /// <param name="model">Model for user</param>
    /// <returns>User detail info model</returns>
    Task<IBaseResponse<UserDetailDto>> AddUserAsync(IUserShortInfo model);

    /// <summary>
    ///  Get user by Id
    /// </summary>
    /// <param name="userId">User Id</param>
    /// <returns>User detail info model</returns>
    Task<IBaseResponse<UserDetailDto>> GetUserByIdAsync(int userId);

    /// <summary>
    ///   Delete an user
    /// </summary>
    /// <param name="userId">Project Id</param>
    /// <returns>Deleted entity dto</returns>
    Task<IBaseResponse<DeletedEntityDto>> DeleteUserAsync(int userId);

    /// <summary>
    ///   Update an user
    /// </summary>
    /// <param name="userId">UserId</param>
    /// <param name="model">Model for user update</param>
    /// <returns>User detail info model</returns>
    Task<IBaseResponse<UserDetailDto>> UpdateUserAsync(int userId, IUserShortInfo model);

    /// <summary>
    ///   Get all users
    /// </summary>
    /// <returns>List users</returns>
    Task<IBaseResponse<List<UserDetailDto>>> GetAllUsersAsync(); 
}
