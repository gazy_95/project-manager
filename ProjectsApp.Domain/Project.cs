﻿
namespace ProjectsApp.Domain;

public class Project
{
	public Project()
	{
		Name = string.Empty;
		СustomerCompanyName = string.Empty;
		ExecutorCompanyName = string.Empty;
	}

	public int Id { get; set; }
	public string Name { get; set; }
	public string СustomerCompanyName { get; set; }
	public string ExecutorCompanyName { get; set; }
	public int Priority { get; set; }
	public DateTime StartDate { get; set; }
	public DateTime EndDate { get; set; }
	public int ProjectManagerId { get; set; }
	public User ProjectManager { get; set; } = null!;
	public List<ProjectParticipants>? AssignedEmployees { get; set; }
}
