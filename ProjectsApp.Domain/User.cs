﻿using Microsoft.AspNetCore.Identity;

namespace ProjectsApp.Domain;

public class User : IdentityUser<int>
{
	public User()
	{
		Name = string.Empty;
		Surname = string.Empty;
		CreatedDate = DateTime.Now;
	}
	public string Name { get; set; }
	public string Surname { get; set; }
	public string? Patronymic { get; set; }
	public DateTime Birthday { get; set; }
	public DateTime CreatedDate { get; set; }
	public List<ProjectParticipants>? AssignedProjects { get; set; }

}
