﻿

namespace ProjectsApp.Domain;

public class ProjectParticipants
{
	public int Id { get; set; }
	public int ProjectId { get; set; }
	public Project Project { get; set; } = null!;
	public int UserId { get; set; }
	public User Employee { get; set; } = null!;
}
