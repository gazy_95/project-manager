﻿
using ProjectsApp.Common.Enums;
using ProjectsApp.Common.Response.Interfaces;

namespace ProjectsApp.Common.Response;

/// <summary>
///  
/// </summary>
/// <typeparam name="T">Model(entity)</typeparam>
public class BaseResponse<T> : IBaseResponse<T>
{
	public BaseResponse()
	{
		Description = string.Empty;
	}

	/// <summary>
	///   Description
	/// </summary>
	public string Description { get; set; }

	/// <summary>
	///  StatusCode
	/// </summary>
	public StatusCode StatusCode { get; set; }

	/// <summary>
	///   Model(entity)
	/// </summary>
	public T? Value { get; set; }
}
