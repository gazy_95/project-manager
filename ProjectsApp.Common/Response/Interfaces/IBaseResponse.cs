﻿using ProjectsApp.Common.Enums;

namespace ProjectsApp.Common.Response.Interfaces;

/// <summary>
///  Base response
/// </summary>
public interface IBaseResponse<T>
{
	/// <summary>
	///   Description
	/// </summary>
	string Description { get; set; }

	/// <summary>
	///  StatusCode
	/// </summary>
	StatusCode StatusCode { get; set; }

	/// <summary>
	///   Model(entity)
	/// </summary>
	T? Value { get; set; }
}
