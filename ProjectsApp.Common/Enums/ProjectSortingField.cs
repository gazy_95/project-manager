﻿
namespace ProjectsApp.Common.Enums;

/// <summary>
///   Project sorting field
/// </summary>
public enum ProjectSortingField
{
	/// <summary>
	///     Priority
	/// </summary>
	Priority = 0,

	/// <summary>
	///    Сustomer company name
	/// </summary>
	СustomerCompanyName = 1,

	/// <summary>
	///   Executor company name
	/// </summary>
	ExecutorCompanyName = 2,

	/// <summary>
	///     Contract name
	/// </summary>
	Name = 3,

    /// <summary>
    ///     Start date
    /// </summary>
    StartDate = 4
}
