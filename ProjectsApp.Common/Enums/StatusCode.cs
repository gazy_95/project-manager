﻿namespace ProjectsApp.Common.Enums;

/// <summary>
///  Status Code
/// </summary>
public enum StatusCode
{
	/// <summary>
	///   Project not found status code
	/// </summary>
	ProjectNotFound = 10,

	/// <summary>
	///  User not found status code
	/// </summary>
	UserNotFound = 20,

	/// <summary>
	///  OK status code
	/// </summary>
	Ok = 200,

	/// <summary>
	///  NotFound status code
	/// </summary>
	NotFound = 404,

	/// <summary>
	///   InternalServerError status code
	/// </summary>
	InternalServerError = 500
}
